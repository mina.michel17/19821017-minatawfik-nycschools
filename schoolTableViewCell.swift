//
//  schoolTableViewCell.swift
//  19821017-minatawfik-NYCSchools
//
//  Created by mina tawfik on 10/27/18.
//  Copyright © 2018 mina tawfik. All rights reserved.
//

import UIKit

class schoolTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
