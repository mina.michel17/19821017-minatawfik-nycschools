//
//  resultViewController.swift
//  19821017-minatawfik-NYCSchools
//
//  Created by mina tawfik on 10/28/18.
//  Copyright © 2018 mina tawfik. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class resultViewController: UIViewController {
    
    
    @IBOutlet weak var SatWritingScore: UILabel!
    @IBOutlet weak var SatReadingScore: UILabel!
    @IBOutlet weak var SatMathScore: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    var selecteSchoolName:String = ""
    
    let URL_MAIN = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
    var parameters:[String : AnyObject] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        parameters = ["school_name" : selecteSchoolName.uppercased()] as [String : AnyObject]
        
        fetchSchoolResult()
        // Do any additional setup after loading the view.
    }
    func fetchSchoolResult()
    {
        DispatchQueue.main.async {
            
            Alamofire.request(self.URL_MAIN, method: .get, parameters:  self.parameters, encoding:URLEncoding.queryString , headers: nil).responseJSON(completionHandler: { response in
                switch response.result{
                case .success(let value):
                    let jason = JSON(value)
                    if jason.isEmpty == false
                    {
                self.schoolNameLabel.text = jason[0]["school_name"].stringValue
                self.SatMathScore.text = jason[0]["sat_math_avg_score"].stringValue
                self.SatReadingScore.text = jason[0]["sat_critical_reading_avg_score"].stringValue
                self.SatWritingScore.text = jason[0]["sat_writing_avg_score"].stringValue
                        
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Their is no Data for this record", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        
                        self.present(alert, animated: true)
                    }
                    

                case .failure(let error):
                    print(error.localizedDescription)
                    
                }
            })
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
