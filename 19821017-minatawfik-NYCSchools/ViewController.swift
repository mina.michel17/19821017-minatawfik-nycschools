//
//  ViewController.swift
//  19821017-minatawfik-NYCSchools
//
//  Created by mina tawfik on 10/26/18.
//  Copyright © 2018 mina tawfik. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var schoolTableView: UITableView!
    var selectedSchool_Name:String = ""
    var schoolData : [schoolModel] = []
     let URL_MAIN = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        schoolTableView.register(UINib(nibName: "schoolTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell") // loading the Xib file
        schoolTableView.delegate = self
        schoolTableView.dataSource = self
        fetchSchoolData()
       
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func fetchSchoolData() // fetching the data from the server and maping jason to model 
    {
        DispatchQueue.main.async {
            
            Alamofire.request(self.URL_MAIN).responseJSON(completionHandler: { response in
             switch response.result{
                 case .success(let value):
                    let jason = JSON(value)
                    
                    for (_,subJson):(String, JSON) in jason {
                        let school = schoolModel(schoolName: subJson["school_name"].string!)
                        self.schoolData.append(school)
                        
                }
                    self.schoolTableView.reloadData()

             case .failure(let error):
                print(error.localizedDescription)
                
                }
                
            })
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! schoolTableViewCell
        cell.schoolNameLabel.text =  schoolData[indexPath.row].schoolName
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
         selectedSchool_Name = schoolData[indexPath.row].schoolName
        performSegue(withIdentifier: "schoolResult", sender: self)
       
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "schoolResult"
        {
            if let destinationVC = segue.destination as? resultViewController {
    
                destinationVC.selecteSchoolName = selectedSchool_Name.uppercased()
            }
        }
    }
}



